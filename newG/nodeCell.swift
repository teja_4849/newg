//
//  nodeCell.swift
//  newG
//
//  Created by MALLOJJALA PAVAN TEJA on 4/2/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//
import UIKit

class nodeCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var license: UILabel!
    
    @IBOutlet weak var permissions: UILabel!
    @IBOutlet weak var descripiton: UILabel!
    @IBOutlet weak var openIssueCount: UILabel!
    
    private var detailsVM: DetailsViewModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    fileprivate func addShadow() {
        self.layer.shadowOffset = CGSize(width: 1, height: 0)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.25
        
    }
    func configure(detailsVM: DetailsViewModel) {
        self.detailsVM = detailsVM
        addShadow()
        name.text = detailsVM.name
        license.text = detailsVM.license
        permissions.text = detailsVM.permissions
        descripiton.text = detailsVM.descripiton
        openIssueCount.text = detailsVM.openIssueCount
    }
    
    func configureFromDB(node: Nodes) {
        name.text = node.name
        license.text = node.license
        permissions.text = node.permissions
        descripiton.text = node.descripiton
        openIssueCount.text = node.openIssueCount
    }
    
}

