//
//  Nodes+CoreDataProperties.swift
//  newG
//
//  Created by MALLOJJALA PAVAN TEJA on 4/2/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//
//

import Foundation
import CoreData


extension Nodes {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Nodes> {
        return NSFetchRequest<Nodes>(entityName: "Nodes")
    }

    @NSManaged public var descripiton: String?
    @NSManaged public var license: String?
    @NSManaged public var permissions: String?
    @NSManaged public var openIssueCount: String?
    @NSManaged public var name: String?

}
